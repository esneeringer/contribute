<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Advertisement;
use App\Services\AdvertisementService;

class AdController extends Controller
{

  protected $advertisementService;


  public function __construct(AdvertisementService $advertisementService)
  {
    $this->advertisementService = $advertisementService;
  }

  //get ad by id
  public function getAd($id)
  {
    return $this->advertisementService->getAdById($id);
  }

  //get ad content by id
  public function getAdContent($id)
  {
    return $this->advertisementService->getAdContent($id);
  }

  //create ad
  public function createAd(Request $request)
  {
    $response = $this->advertisementService->createAd($request);
    return $response;
  }

  //update ad
  public function updateAdById(Request $request, $id)
  {
    return $this->advertisementService->updateAdById($request, $id);
  }
}
