<?php

namespace App\Services;

use App\Advertisement as Advertisement;

class AdvertisementService
{
    public function getAdById($id)
    {
      return Advertisement::where('id', $id)->first();
    }

    public function getAdContent($id)
    {
      return Advertisement::where('id', $id)->value('advertisement_content');
    }

    public function createAd($request)
    {
      $ad = new Advertisement;
      $ad->advertisement_content = $request->advertisement_content;
      $ad->save();
    }

    public function updateAdById($request, $id)
    {
      $ad = new Advertisement;
      $advertisementContent = $request->advertisement_content;
      Advertisement::where('id', $id)->update(['advertisement_content' => $advertisementContent]);
    }
}
