#!/bin/bash

echo "Start Shell Provisioning"

#navigate to app directory
echo "navigate to app directory"
cd ../../var/www/

echo "create .env file"
cp .env.example .env

#composer update and install
echo "Update composer"
composer self-update

echo "Install composer dependancies"
composer install

#generate app key
echo "Generate app secret key"
php artisan key:generate
