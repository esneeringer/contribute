<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

//Get Ad by Id
Route::get('/ad/id/{id}', 'AdController@getAd');
Route::get('/ad/content/id/{id}', 'AdController@getAdContent');
Route::post('/ad', 'AdController@createAd');
Route::put('/ad/id/{id}','AdController@updateAdById');
